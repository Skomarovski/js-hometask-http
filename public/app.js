'use strict';

function addUserFetch(userInfo) {
  let requestOptions = {
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(userInfo)
  }

  return fetch('/users', requestOptions)
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(`\nКод ошибки: ${response.status},\n` +
                        `Текст ошибки: ${response.statusText}`);
      }
    })
    .then(user => user.id)
    .catch(error => alert(error))
}

function getUserFetch(id) {
  let requestOptions = {
    method: 'GET',
    headers: {
      'Content-type': 'application/json'
    }
  }

  return fetch(`/users/${id}`, requestOptions)
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(`\nКод ошибки: ${response.status},\n` +
                        `Текст ошибки: ${response.statusText}`);
      }
    })
    .then(user => user)
    .catch(error => alert(error))
}

function addUserXHR(userInfo) {
  const method = 'POST';
  const url = '/users'
  const xhr = new XMLHttpRequest();

  return new Promise((res, rej) => {
    xhr.open(method, url);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.responseType = 'json';
    xhr.send(JSON.stringify(userInfo));

    xhr.onload = () => {
      if (xhr.status.toString().split(0,1)[0] === "2") {
        res(xhr.response.id);
      } 
      else {
        rej(new Error(`\nКод ошибки: ${res.status},\n` +
                      `Текст ошибки: ${res.statusText}`));
      }
    };
  })
  .catch(error => alert(error))
}

function getUserXHR(id) {
  const method = 'GET';
  const url = `/users/${id}`;
  const xhr = new XMLHttpRequest();

  return new Promise((res, rej) => {
    xhr.open(method, url);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.responseType = 'json';
    xhr.send();

    xhr.onload = () => {
      if (xhr.status.toString().split(0,1)[0] === "2") {
        res(xhr.response);
      } 
      else {
        rej(new Error(`\nКод ошибки: ${res.status},\n` +
                      `Текст ошибки: ${res.statusText}`));
      }
    }
  })
  .catch(error => alert(error))
}

function checkWork() {
  addUserXHR({ name: 'Alice', lastname: 'FetchAPI' })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

checkWork();